var mname;
var incloc;

function ipaddr(json) {
    mname = json.ip + "@imdead.esy.es:";
    incloc=document.getElementById("includer");
    machinename();
}

function entries(entry) {
	console.log(entry.toLowerCase());
    switch (entry.toLowerCase()) {
    	case "-h":
    		control("1"); 
    		break;
    	case "clear":
    		incloc.innerHTML="";
    		machinename();
    		break;
    	case "help":
    		control("1");
    		break;
    	case "aboutme":
    		control("2");
    		break;
    	case "projects":
    		control('3');
    		break;
        default: 
        	control("0");
        	break;
    }
}

function machinename() {
    fspan = document.createElement("span");
    fspan.className = "host";
    fspan.textContent = mname;
    console.log(mname);
    sspan = document.createElement("span");
    textfield = document.createElement("input");
    textfield.className = "infield";
    textfield.spellcheck = "false";
    sspan.appendChild(textfield);
    fspan.appendChild(sspan);
    incloc.appendChild(fspan);
    textfield.focus();
    textfield.addEventListener("keydown",function(event){
    	if(event.which==13 || event.keyCode==13){
    		textfield.setAttribute("Disabled","true");
    		entries(textfield.value);
    	}
    });
}

function control(val) {
    ndiv = document.createElement("div");
    ndiv.className = "txt"
    console.log(val);
    if (val == "0") {
        ndiv.innerHTML = "The Command is not valid. use -h for help";
        incloc.appendChild(ndiv);
    }
    else if(val=="1"){
    	ndiv.innerHTML = "Possible Commands: -h, help, aboutme, projects";
    	incloc.appendChild(ndiv);
    }
    else if(val=="2"){
    	ndiv.innerHTML+='Ima Engineer, (re) Cracker, Developer, Privacy freak, and a person whois interested in security.<br/>';
    	ndiv.innerHTML+='My Twitter:<a href="https://twitter.com/xbluepie" target="_blank" style="color: white;text-decoration: none;">BluePie</a>';
    	incloc.appendChild(ndiv);
    }
    else if(val == "3"){
    	ndiv.innerHTML+='Bitbucket:<a href="https://bitbucket.org/Bluepie" target="_blank" style="color: white;text-decoration: none;">BluePie</a></br>';
    	ndiv.innerHTML+='Github:<a href="https://github.com/gopinath001" target="_blank" style="color: white;text-decoration: none;">Gopinath001</a></br>';
    	incloc.appendChild(ndiv);
    }
    
    machinename();
}